# Method

1. Extract all names from here: [http://www.massey.ac.nz/massey/learning/colleges/college-of-sciences/about/natural-sciences/staff-by-cluster.cfm](http://www.massey.ac.nz/massey/learning/colleges/college-of-science\
s/about/natural-sciences/staff-by-cluster.cfm)
2. Group into c1: Math, Stats, CompSci, InfoTech and c2: Bio, Ecol, Evol, Chem, Physics to get somehow equal numbers
3. Remove Tutors and Technicians
4. Unique names and sort
5. Shuffle names


```bash
cat c1-sorted.txt | gshuf > c1-shuffled.txt
cat c2-sorted.txt | gshuf > c2-shuffled.txt
```

